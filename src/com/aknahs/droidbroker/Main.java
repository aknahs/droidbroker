package com.aknahs.droidbroker;

import com.aknahs.graphviz.GraphGenerator;
import com.aknahs.honeypots.PackageCategories;

import java.io.File;
import java.io.IOException;

import org.apache.commons.cli.*;

public class Main {

    public static final String BINARY_NAME = "DroidBroker";
    public static final String VERSION = "0.2.2";
    public static final String APK_PATH = "apks/";
    public static String path = APK_PATH;

    public static boolean hasDependencies() {

        try {
            Process neato = Runtime.getRuntime().exec(
                    new String[] { "neato", "-V" });

            neato.waitFor();

            int exitVal = neato.exitValue();
            return exitVal == 0 ? true : false;
        } catch (IOException e) {
            System.out
                    .println("Couldnt verify the existence of neato [IOException]");
        } catch (InterruptedException e) {
            System.out
                    .println("Couldnt verify the existence of neato [InterruptedException]");
        }
        return false;

    }

    private static void printHelp(Options options){
        String header = BINARY_NAME + " parses the apktool output folder to retrieve app's call graphs.\n\n";

        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp(BINARY_NAME, header, options, "\nMore tools in https://github.com/4knahs.", true);
    }

    private static void wrongCommandLine(Options options, String message){
        System.err.println("[ERROR] " + message);
        printHelp(options);
        System.exit(-1);
    }

    private static boolean processMainArgs(String[] args) {
        Options options = new Options();
        CommandLineParser parser = new DefaultParser();

        String printArgs = "";
        for(String a : args)
            printArgs += " " + a;

        //System.out.println("Commandline arguments:" + printArgs);

        options.addOption("a", "apktool-path", true, "Path to the apktool binary.");
        options.addOption("A", "apps-path", true, "Path to the apks folder.");
        options.addOption("d", "enable-dot", false, "Enables dot generation.");
        options.addOption("P", "enable-plots", false, "Tries to plot dot files as png.");
        options.addOption("D", "enable-debug", false, "Enables debug messages.");
        options.addOption("t", "enable-tags", false, "Enable tags from android.packages and ignore files from android.ignore.");
        options.addOption("c", "compress", false, "Outputs a compressed version of the app representation.");
        options.addOption("p", "phantom-class", true, "Class and methods used has phantom references (honeyPots). DEPRECATED.");
        options.addOption("H", "known-phantom-classes", false, "Loads known phantom references. DEPRECATED.");
        options.addOption("v", "version", false, "shows the droidbroker version.");
        options.addOption("h", "help", false, "Prints this message.");

        try{
            CommandLine cmd = parser.parse(options, args);

            File at = new File("apktool");

            DroidBroker.apktoolPath = cmd.getOptionValue("a", at.getAbsolutePath());
            path = cmd.getOptionValue("A", APK_PATH);
            DroidBroker.doGraphs = cmd.hasOption("d");

            System.out.println("Graphs: " + DroidBroker.doGraphs);
            System.out.println("Apktool: " + DroidBroker.apktoolPath);
            System.out.println("Apps: " + path);

            if (DroidBroker.doGraphs && cmd.hasOption("P")) {
                GraphGenerator._autoPloting = true;
                System.out.println("Turned on automated graph plotting.");
            } else if (cmd.hasOption("P")) {
                System.err.println("-P requires -d (enable-dot)");
                System.exit(-1);
            }

            DroidBroker._debug = cmd.hasOption("D");

            if (cmd.hasOption("t")) {
                PackageCategories.loadKnownPackageTags();
                PackageCategories.loadIgnored();
                System.out.println("Loaded known tags and ignored classes.");
            }

            DroidBroker._compress = cmd.hasOption("c");

            if (cmd.hasOption("p")) {
                // PackageCategories.loadHoneyPotClasses();
                System.err.println("The honeypot/phantom functionality has been deactivated!");
                System.exit(-1);
            }

            if (cmd.hasOption("H")) {
                // String cls = args[processedArgs + 1];
                // String[] mths = args[processedArgs + 2].split(" ");

                System.err.println("The honeypot/phantom functionality has been deactivated!");
                // System.out.println("Registering know class : " + cls);
                // SmaliHoneyPots.registerKnownClass(cls, mths);
                System.exit(-1);
            }

            if (cmd.hasOption("v")) {
                System.out.println("DROIDBROKER v" + VERSION);
                System.exit(0);
            }

            if (cmd.hasOption("h")) {
                printHelp(options);
                System.exit(0);
            }

        } catch (ParseException e) {
            wrongCommandLine(options, "Could not parse your commandline arguments");
        }

        return true;

    }

    public static void main(String[] args) {

        if (!processMainArgs(args))
            return;

        if (DroidBroker.doGraphs) {
            if (!hasDependencies())
                return;
        }

        //if (!_debug)
        //	SmaliLogger.disableLog();

        File[] files = new File(path).listFiles();

        if (files == null) {
            System.out.println("[ERROR] : There was no apks folder.");
            return;
        }

        DroidBroker.showFiles(files);

        DroidBroker.processAllAPK(path);

        System.out.println("Done");

    }
}
