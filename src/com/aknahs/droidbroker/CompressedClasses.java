package com.aknahs.droidbroker;

import java.util.HashMap;

import com.google.gson.annotations.Expose;

public class CompressedClasses {
    @Expose
    public HashMap<Integer,CompressedClass> _classes = new HashMap<Integer,CompressedClass>();
    @Expose
    public HashMap<String,Integer> _ids = new HashMap<String,Integer>();
    public HashMap<Integer,String> _names = new HashMap<Integer,String>();
}
