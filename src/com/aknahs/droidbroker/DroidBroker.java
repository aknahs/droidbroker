package com.aknahs.droidbroker;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Scanner;

import com.aknahs.exporters.SmaliApkExporter;
import com.aknahs.graphviz.GraphGenerator;
import com.aknahs.honeypots.PackageCategories;
import com.aknahs.honeypots.SmaliHoneyPots;
import com.aknahs.log.SmaliLogger;
import com.aknahs.manifest.ManifestDirectives;
import com.aknahs.manifest.ManifestRepresentation;
import com.aknahs.smali.opcodes.SmaliDirectives;
import com.aknahs.smali.opcodes.SmaliOperations;
import com.aknahs.smali.representations.SmaliApkRepresentation;
import com.aknahs.smali.representations.SmaliClassRepresentation;
import com.aknahs.smali.representations.SmaliFieldRepresentation;
import com.aknahs.smali.representations.SmaliLinkRepresentation;
import com.aknahs.smali.representations.SmaliMethodRepresentation;
import com.aknahs.smali.representations.SmaliNameEncoding;
import com.aknahs.smali.representations.SmaliRepresentation;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

//For measuring Object sizes
//import java.lang.instrument.Instrumentation;

public class DroidBroker {

    public static boolean _debug = false;
    public static boolean _compress = false;
    public static volatile boolean doGraphs = true;
    public static String apktoolPath = "APKTOOL_NOT_DEFINED";

    private static ArrayList<SmaliMethodRepresentation> _traversedMths = new ArrayList<SmaliMethodRepresentation>();

    private static void clearTraversedMethods() {
        if (_traversedMths == null)
            return;

        for (SmaliMethodRepresentation cleanMth : _traversedMths)
            cleanMth._traversed = false;

        _traversedMths.clear();
    }

    public DroidBroker() {
        super();
    }

    public static void showFiles(File[] files) {
        for (File file : files) {
            // file.getAbsolutePath();
            if (file.isDirectory()) {
                System.out.println("Directory: " + file.getName());
                // showFiles(file.listFiles()); // Calls same method again.
            } else {
                System.out.println("File: " + file.getName());
            }
        }
    }

    public static void doLog(String message) {
        if (_debug)
            doLog("DEBUG", message);
    }

    public static void doLog(String type, String message) {
        System.out.println(type + " | " + message);
    }

    private static SmaliRepresentation processSmaliLine(
            SmaliApkRepresentation apk, SmaliClassRepresentation cls,
            SmaliMethodRepresentation mth, String line, short counter) {
        Scanner scanner = new Scanner(line.trim());
        scanner.useDelimiter(" ");

        if (!scanner.hasNext()) {
            scanner.close();
            return null;
        }

        String type = scanner.next();
        doLog("Scanning : " + type);
        // class, methods, fields and other annotations
        if (SmaliDirectives.isDirective(type)) {

            switch (SmaliDirectives.convertDirective(type)) {
                case CLASS:
                    return processClass(apk.getNameEncoding(), scanner, line); // closes
                // scanner
                case METHOD:
				/* closes scanner */
                    return processMethod(apk.getNameEncoding(), scanner, line, cls,
                            counter);
                case SUPER:
				/* closes scanner */
                    return processSuper(apk.getNameEncoding(), scanner, line, cls);
                case FIELD:
                    return processField(apk.getNameEncoding(), scanner, line, cls);
                case LOCALS:
                    String[] splits = line.trim().split(" ");
                    mth.setLocals(new Short(splits[1]));
                default:
                    doLog("Ignored directive : " + type);
                    break;
            }

        } else {
            doLog("Is not a directive : " + type);
            // TODO: Detect reflection -> is it really needed? afterwards it is
            // the real class invoke no?
            switch (SmaliOperations.convertOperation(type)) {
                case INVOKE:
                    return processInvoke(apk.getNameEncoding(), scanner, line, cls,
                            mth);
                case NEW_INSTANCE:
                    return processNewInstance(scanner, line, cls, mth);
                default:
                    break;
            }
        }

        scanner.close();
        return null;
    }

    public static SmaliRepresentation processSuper(
            SmaliNameEncoding nameSchema, Scanner scanner, String line,
            SmaliClassRepresentation cls) {

        doLog("Super looking in : " + line);
        String superClass = "";

        while (scanner.hasNext()) {
            String attr = scanner.next();
            if (attr.charAt(0) == 'L') {
                Scanner innerScan = new Scanner(attr.substring(1));
                innerScan.useDelimiter(";");
                superClass = innerScan.next().replace('/', '.');

                cls.setSuperClassName(superClass);

                innerScan.close();

                doLog("Registering superclass : " + superClass);
                break;
            }
        }

        scanner.close();
        return null;
    }

    public static SmaliLinkRepresentation processInvoke(
            SmaliNameEncoding nameSchema, Scanner scanner, String line,
            SmaliClassRepresentation cls, SmaliMethodRepresentation mth) {

        doLog("Invoke looking in : " + line);

        String invokedClass = "";
        String invokedMethod = "";
        while (scanner.hasNext()) {
            String attr = scanner.next();
            if (attr.charAt(0) == 'L') {
                Scanner innerScan = new Scanner(attr.substring(1));
                innerScan.useDelimiter(";");
                invokedClass = innerScan.next().replace('/', '.');
                String aux = innerScan.next().substring(2);
                invokedMethod = aux.substring(0, aux.indexOf('('));
                innerScan.close();
                scanner.close();

                doLog("Registering invoke : " + invokedClass + "."
                        + invokedMethod);

                return new SmaliLinkRepresentation(nameSchema, invokedClass,
                        invokedMethod);
            }
        }

        doLog("Couldnt find invoke");
        scanner.close();
        return null;
    }

    public static SmaliLinkRepresentation processNewInstance(Scanner scanner,
                                                             String line, SmaliClassRepresentation cls,
                                                             SmaliMethodRepresentation mth) {
        doLog("New instance looking in : " + line);

        String invokedClass = "";

        while (scanner.hasNext()) {
            String attr = scanner.next();
            if (attr.charAt(0) == 'L') {
                Scanner innerScan = new Scanner(attr.substring(1));
                innerScan.useDelimiter(";");
                invokedClass = innerScan.next().replace('/', '.');

                // it will have to be initialized and only then we register it
                doLog("Ignored new instance : " + invokedClass);
                innerScan.close();
                scanner.close();
                return null;
            }
        }

        doLog("Couldnt find new instance");
        scanner.close();
        return null;
    }

    public static SmaliFieldRepresentation processField(
            SmaliNameEncoding nameSchema, Scanner scanner, String line,
            SmaliClassRepresentation cls) {

        // System.out.println("Field looking in " + line);
        String fieldName = "";
        Integer modifier = 0;

        while (scanner.hasNext()) {
            String attr = scanner.next();

			/* Maps modifier strings to modifier values (e.g. Modifier.PUBLIC) */
            Integer mod = _modifiers.get(attr);

            if (mod != null) {
                modifier |= mod; // bitwise or of modifier mask
            } else {
                Integer index = attr.indexOf('$');

				/* a:classType */
                index = attr.indexOf(':');
                if (index > 0)
                    fieldName = attr.substring(0, index);
                else
                    doLog("Field unknown attribute : " + attr);

            }
        }

        SmaliFieldRepresentation field = null;

		/* We do not want the this field */
        if (!fieldName.equals("this")) {
            field = new SmaliFieldRepresentation(nameSchema, cls.getClassID(),
                    fieldName, modifier);

            // System.out.println("Processed field : " + field);
        } else {

        }

        return field;
    }

    private static HashMap<String, Integer> _modifiers = new HashMap<String, Integer>();

    static {
        for (Field field : Modifier.class.getDeclaredFields()) {
            String fieldname = field.getName().toLowerCase();
            field.setAccessible(true);
            Integer modifier;
            try {
                modifier = (int) field.get(null);

                _modifiers.put(fieldname, modifier);
            } catch (IllegalArgumentException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    public static SmaliMethodRepresentation processMethod(
            SmaliNameEncoding nameSchema, Scanner scanner, String line,
            SmaliClassRepresentation cls, short counter) {

        doLog("Method looking in : " + line);

        String methodname = "";
        while (scanner.hasNext()) {
            String attr = scanner.next();

            if (attr.contains("(")) {
                methodname = attr;
                break;
            }
        }

        if (methodname.equals("")) {
            doLog("Couldnt find methodname");
            scanner.close();
            return null;
        }

        scanner.close();

        doLog("Registering method : "
                + methodname.substring(0, methodname.indexOf('(')));

        SmaliMethodRepresentation mth = new SmaliMethodRepresentation(
                nameSchema, cls.getClassName(), methodname.substring(0,
                methodname.indexOf('(')));

        mth.setLine(counter);
        return mth;
    }

    public static SmaliClassRepresentation processClass(
            SmaliNameEncoding nameSchema, Scanner scanner, String line) {
        String classname = "";

        while (scanner.hasNext()) {
            String attr = scanner.next();
            if (attr.charAt(0) == 'L') {
                classname = attr;
                break;
            }
        }

        if (classname.equals("")) {
            doLog("Couldnt find classname");
            scanner.close();
            return null;
        }
        scanner.close();

        doLog("Registering class : "
                + classname.substring(1, classname.length() - 1).replace("/",
                "."));
        return new SmaliClassRepresentation(nameSchema, classname.substring(1,
                classname.length() - 1).replace("/", "."));
    }

    public static void parseSmali(SmaliApkRepresentation apk, File file) {
        doLog("Parsing smali : " + file.getAbsolutePath());
        SmaliRepresentation representation;
        SmaliClassRepresentation currentClass = null;
        SmaliMethodRepresentation currentMethod = null;
        short counter = 0;

        try {
            Scanner scanner = new Scanner(file);

            while (scanner.hasNextLine()) {
                representation = processSmaliLine(apk, currentClass,
                        currentMethod, scanner.nextLine(), counter);
                counter++;

                if (representation == null)
                    continue;

                switch (representation.getType()) {
                    case CLASS:
                        currentClass = (SmaliClassRepresentation) representation;
                        apk.registerClass(currentClass);
                        break;
                    case METHOD:
                        currentMethod = (SmaliMethodRepresentation) representation;
                        if (currentClass != null)
                            currentClass.registerMethod(currentMethod);
                        else
                            doLog("[ERROR] Current class was null");
                        break;
                    case LINK:
                        currentMethod
                                .createAccessToAnotherMethod((SmaliLinkRepresentation) representation);
                        break;
                    case FIELD:
                        currentClass
                                .registerField((SmaliFieldRepresentation) representation);
                        break;
                    default:
                        break;
                }
            }

            scanner.close();

        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static void recurSmali(SmaliApkRepresentation apk, File[] files) {
        for (File file : files) {
            // file.getAbsolutePath();
            if (file.isDirectory()) {
                doLog("Directory: " + file.getName());
                recurSmali(apk, file.listFiles()); // Calls same method again.
            } else {

                if (file.getName().contains(".smali")) {
                    parseSmali(apk, file);
                } else
                    doLog("Non smali found: " + file.getName());
            }
        }
    }

    /* Sometimes it might be the case the Manifest has inline directives */
    public static ArrayList<String> getInlineDirectives(String line) {

        int[] dirs = new int[1];
        int dircount = 0;

        for (int i = 0; i < line.length(); i++) {
            char ch = line.charAt(i);
            if (ch == '<') {
                dircount++;
                int[] newdirs = new int[dircount];
                for (int j = 0; j < dircount - 1; j++)
                    newdirs[j] = dirs[j];
                newdirs[dircount - 1] = i;
                dirs = newdirs;
            }
        }

        if (dircount > 1)
            doLog("Found inline directives in Manifest file : " + line);

        ArrayList<String> ret = new ArrayList<String>();

        for (int i = 0; i < dircount; i++) {
            String l = line.substring(dirs[i],
                    (i + 1 > dircount - 1) ? (line.length() - 1)
                            : (dirs[i + 1] - 1));
            ret.add(l);
            doLog("Inline " + i + " : " + l);
        }

        return ret;
    }

    public static ManifestRepresentation readAPKManifest(
            SmaliNameEncoding nameSchema, String apk) {
        ManifestRepresentation man = new ManifestRepresentation(nameSchema);
        String[] splits;
        String currentActivity = null;
        String currentApplication = null;

        try {
            File manifest = new File("./results/" + apk
                    + "/AndroidManifest.xml");
            Scanner scanner = new Scanner(manifest);

            while (scanner.hasNextLine()) {
                String line = scanner.nextLine().trim();

                if (line.length() <= 0)
                    continue;

                if (!ManifestDirectives.isDirective(line))
                    continue;

                ArrayList<String> dirs = getInlineDirectives(line);

                for (String dir : dirs) {

                    splits = dir.split(" ");

                    switch (ManifestDirectives.convertDirective(splits[0])) {

                        case RECEIVER:
                            for (String spl : splits) {
                                if (spl.contains("android:name=")) {
                                    String[] subsplits = spl.split("\"");
                                    String receiver = subsplits[1];

                                    if (receiver.length() > 0
                                            && receiver.charAt(0) == '.') {

                                        receiver = man._apkname.replace('/', '.')
                                                + receiver.replace('/', '.');

                                    } else
                                        receiver = receiver.replace('/', '.');

                                    System.out.println("receiver name found : "
                                            + receiver);
                                    man.registerReceiver(receiver);
                                    break;
                                }
                            }

                            break;

                        case MANIFEST:
                            for (String spl : splits) {
                                if (spl.contains("package=")) {
                                    String[] subsplits = spl.split("\"");
                                    doLog("Package name found : " + subsplits[1]);
                                    man._apkname = subsplits[1];
                                    break;
                                }
                            }
                            break;

                        case ACTION:
                            if (line.contains("android.intent.action.MAIN")) {
                                man._mainActivity = currentActivity;

                                if (currentActivity.length() > 0
                                        && currentActivity.charAt(0) == '.') {

                                    man._mainActivity = man._apkname.replace('/',
                                            '.')
                                            + currentActivity.replace('/', '.');

                                } else
                                    man._mainActivity = currentActivity.replace(
                                            '/', '.');

                                System.out.println("App name = " + man._apkname
                                        + " mainActivity = " + man._mainActivity);
                            }
                            break;

                        case ACTIVITY:

                            for (String spl : splits) {
                                if (spl.contains("android:name=")) {
                                    String[] subsplits = spl.split("\"");
                                    System.out.println("Activity name found : "
                                            + subsplits[1]);
                                    currentActivity = subsplits[1];

								/*
								 * Found unique cases of applications for which
								 * the '.' is not mapped to the package name but
								 * rather to a variation of the application
								 * name. It goes a bit against the android
								 * documentation. E.g:
								 *
								 * Activity name:
								 * .contacts.ContactSuggestedInvitesActivity
								 *
								 * package="com.skype.raider"
								 *
								 * <application ... android:name=
								 * "com.skype.android.app.SkypeApplication"
								 *
								 * .contacts.ContactSuggestedInvitesActivity is
								 * mapped to com.skype.android.app
								 *
								 * = com.skype.android.app.contacts.
								 * ContactSuggestedInvitesActivity
								 *
								 * when according to documentation should be:
								 *
								 * = com.skype.raider.contacts.
								 * ContactSuggestedInvitesActivity
								 *
								 * My guess is that they have a repo for
								 * multiple target platforms and use custom.xml
								 * to handle the building and replacement of
								 * package names.
								 */

								/*
								 * if(currentActivity.charAt(0) == '.'){
								 * if(currentApplication != null){
								 * currentActivity = currentApplication +
								 * currentActivity; System.out.println
								 * ("Refactored activity name to : " +
								 * currentActivity); }else{ currentActivity =
								 * man._apkname + currentActivity;
								 * System.out.println
								 * ("Refactored activity name to : " +
								 * currentActivity); } }
								 */

                                    man.registerActivity(currentActivity.replace(
                                            '/', '.'));
                                    break;
                                }
                            }
                            break;

					/*
					 * We handle application name to replace the '.' annotation
					 * in the activities naming
					 */
                        case APPLICATION:
                            for (String spl : splits) {
                                if (spl.contains("android:name=")) {
                                    String[] subsplits = spl.split("\"");
                                    System.out.println("Application name found : "
                                            + subsplits[1]);
                                    currentApplication = subsplits[1];
                                    break;
                                }
                            }
                            break;

                        default:
                            break;
                    }
                }

            }
            scanner.close();

        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }

        return man;
    }

    private static void processInnerClasses(SmaliApkRepresentation apk) {
        for (SmaliClassRepresentation smaliCls : apk.getClasses()) {
            String clsName = smaliCls.getClassName();

            int index = clsName.indexOf('$');

			/* This is an inner class */
            if (index > 0) {
                SmaliClassRepresentation parentClass = apk.getClass(clsName
                        .substring(0, index));
                if (parentClass != null) {
                    doLog("Registering innerclass " + smaliCls.getClassName()
                            + " in class " + parentClass.getClassName());
                    parentClass.registerInnerClass(smaliCls.getClassID());
                } else {
                    System.out.println("XXX Failed Registering innerclass "
                            + smaliCls.getClassName() + " in class "
                            + clsName.substring(0, index));
                }
            }
        }
    }

    public static SmaliApkRepresentation processApk(String path, String apk) {
        try {

            System.out.println("processApk : " + path + "/" + apk + ".apk");
            File file = new File(path + "/" + apk + ".apk");
            System.out.println("Path to apktool : " + apktoolPath);

            doLog(file.getAbsolutePath());

			/*
			 * Retrieves the manifest and resources only.
			 * Why? Decoding resources often fails but the manifest is still generated.
			 * Unfortunately, when the resources fail, the sources are not decompiled.
			 * So we need to do both steps in separation to assure it always runs.
			 * One is done with -s = no sources; and other with -r = no raw resources
			 * */
            try {
                Process apktool = Runtime.getRuntime().exec(
                        new String[] { apktoolPath, "d",
                                file.getAbsolutePath(), "-f", "-s", "-o",
                                "results/" + apk });

                apktool.waitFor();
            } catch (IOException e) {
                doLog("Could not access apktool. Is it instaled? perhaps trying : java -jar trapmaster-broker -a $APKTOOL_PATH");
                return null;
            }

            SmaliNameEncoding nameSchema = new SmaliNameEncoding();

            ManifestRepresentation man = readAPKManifest(nameSchema, apk);

            if (man == null) {
                doLog("Failed reading manifest.");
                return null;
            } else
                doLog("Done processing manifest");

            String apkname = man._apkname;

			/*
			 * Retrieves the decompiled sources.
			 */
            try {
                Process apktool = Runtime.getRuntime().exec(
                        new String[] { apktoolPath, "d",
                                file.getAbsolutePath(), "-f", "-r", "-o",
                                "results/" + apk });

                apktool.waitFor();
            } catch (IOException e) {
                doLog("Could not access apktool. Is it instaled? perhaps trying : java -jar trapmaster-broker -a $APKTOOL_PATH");
                return null;
            }

            if (apkname != null) {
                File[] files = new File("./results/" + apk + "/smali/")
                        .listFiles();

                if (files == null) {
                    doLog("Unexpected non-existing folder : " + "./results/"
                            + apk + "/smali/");
                    return null;
                }

                doLog("Generating honey pots...");
                SmaliHoneyPots honeyPots = new SmaliHoneyPots(nameSchema);

                doLog("Generating SmaliApkRepresentation...");
                SmaliApkRepresentation newApk = new SmaliApkRepresentation(
                        honeyPots, nameSchema, man);

                doLog("Executing recurSmali...");
				/* parses the apk smali files and fills the newApk */
                recurSmali(newApk, files);

                doLog("Executing processReverseLinks...");
				/* generates the reverse links between methods */
                processReverseLinks(honeyPots, newApk);

                doLog("Executing propagateTagsForAllHoneyPots...");
				/* uses the reverse links to propagate tags */
                propagateTagsForAllHoneyPots(honeyPots, newApk);

                doLog("Executing processInnerClasses...");
				/* identify innerclasses */
                processInnerClasses(newApk);

                doLog("Executing processShortActivityNames...");
                processShortActivityNames(newApk);

                return newApk;
            } else
                doLog("Unexpected null apkname!");

        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        doLog("Could not process APK");
        return null;
    }

    public static void processShortActivityNames(SmaliApkRepresentation newApk) {
        HashMap<Integer, Integer> newMappings = new HashMap<Integer, Integer>();
		/* Identify '.' notation in activity naming */
        for (Integer activityID : newApk.getManifest()._activities) {
            String activityName = newApk.getNameEncoding().getName(activityID);

            if (activityName.charAt(0) == '.') {
                String attemptiveName = newApk.getApkName() + activityName;

                if (newApk.getNameEncoding().getID(attemptiveName) != null) {
                    newMappings.put(activityID,
                            newApk.getNameEncoding().getID(attemptiveName));
                    continue;
                }

                String currentGuess = null;
                Integer minDiff = Integer.MAX_VALUE;

                for (SmaliClassRepresentation cls : newApk.getClasses()) {
                    if (cls.getClassName().length() - activityName.length() < 0)
                        continue;

                    String classSubString = cls.getClassName()
                            .substring(
                                    cls.getClassName().length()
                                            - activityName.length(),
                                    cls.getClassName().length());

                    if (classSubString.equals(activityName)) {
                        System.out.println("ASSUMING ACTIVITY NAME : "
                                + cls.getClassName());
                        newMappings.put(activityID, cls.getClassID());
                        break;
                    }
                }
            }
        }

        for (Integer key : newMappings.keySet()) {
            System.out.println("Removing activity : "
                    + newApk.getNameEncoding().getName(key));
            newApk.getManifest()._activities.remove(key);
            System.out.println("Replacing by : "
                    + newApk.getNameEncoding().getName(newMappings.get(key)));
            newApk.getManifest()._activities.add(newMappings.get(key));
        }

    }

    public static void processAllAPK(String path) {
        int counter = 0;
        File[] files = new File(path).listFiles();
        String appName;

        for (File file : files) {
            if (!file.isDirectory() && file.getName().contains(".apk")) {
                counter++;

                appName = file.getName().substring(0,
                        file.getName().indexOf(".apk"));

                System.out.println("Processing : " + appName);

                SmaliApkRepresentation apk = processApk(path, appName);

                if (apk == null) {
                    System.out.println("Failed to process APK");
                    continue;
                } else
                    System.out.println("###Found receivers : "
                            + apk.getManifest().getReceivers().size());

                if (doGraphs) {
                    System.out.print("Generating dot file(results/"
                            + apk.getApkName() + ".dot" + ")...");
                    GraphGenerator.generateGephiGraph(apk);
                    System.out.println("done!");

                    System.out.print("Generating honey pot files(results/"
                            + apk.getApkName() + "<Class>.dot" + ")...");
                    GraphGenerator.generateReverseGraphPerClassForWholeAPK(apk);
                    System.out.println("done!");

                    System.out.print("Generating Global TAGS...");
                    GraphGenerator.generateTagsGraph(apk);
                    System.out.println("done!");

                    System.out.print("Generating per TAGS...");
                    for (String tag : PackageCategories.getTags()) {
                        GraphGenerator.generateTagGraph(apk, tag);
                    }
                    System.out.println("done!");
                }

                SmaliApkExporter.generateJSON("results/" + appName, apk);

                CompressedClasses cmp = new CompressedClasses();

                if(_compress){
                    for(SmaliClassRepresentation cls : apk.getClasses()){
                        CompressedClass c = new CompressedClass(cls.getClassID());
                        cmp._classes.put(cls.getClassID(), c);
                        for(SmaliMethodRepresentation mth : cls.getMethods()){
                            for(SmaliLinkRepresentation lnk : mth.getLinks()){
                                cmp._ids.put(lnk.getNameEncoding().getName(lnk.getLinkClass()), lnk.getLinkClass());
                                c._interactClasses.add(lnk.getLinkClass());

                                System.out.println("Compressed : " + lnk.getNameEncoding().getName(lnk.getLinkClass()) + "." + lnk.getLinkName());

                            }
                        }
                    }
                }

                generateJSON("compressed", cmp);
            }
        }

        if (counter == 0)
            doLog("There were no apk files");
    }

    public static void generateJSON(String name, Object apk) {
        try {
            System.out.print("SmaliApkExporter : " + "Generating json file(" + name
                    + ".json" + ")...");
            PrintWriter writer = new PrintWriter(
                    name + ".json", "UTF-8");
            Gson gson = new GsonBuilder()
                    .excludeFieldsWithoutExposeAnnotation()
                    .setPrettyPrinting()
                    .create();
            String json = gson.toJson(apk);
            writer.println(json);
            writer.close();
            SmaliLogger.log("SmaliApkExporter","done!");

        } catch (FileNotFoundException | UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /*
     * This method is used after processReverseLinks to propagate the
     * SmaliNameEncodingtags through the existing links. Having the reverse
     * links is important to simplify its propagation from the SmaliHoneyPots.
     */
    private static void propagateTagsForAllHoneyPots(SmaliHoneyPots honeyPots,
                                                     SmaliApkRepresentation apk) {
        for (SmaliClassRepresentation cls : honeyPots.getSmaliKnownClasses()) {
            for (SmaliMethodRepresentation honeyMth : cls.getMethods()) {
                clearTraversedMethods(); // Clear the traversed properties
                propagateTagsForHoneyPot(apk, honeyMth._accesses.getTags(),
                        honeyMth);
            }
        }

        clearTraversedMethods();
    }

    private static void propagateTagsForHoneyPot(SmaliApkRepresentation apk,
                                                 HashSet<String> tags, SmaliMethodRepresentation mth) {

        // System.out.println("----->" + "propagate tags");
        if (mth == null || mth._traversed) {
            // System.out.println("----->" + "Method was already traversed");
            return;
        } else {
            // System.out.println("----->" + "traversed method");
            mth._traversed = true;
            _traversedMths.add(mth);
            mth._accesses.registerTags(tags); // register tags
        }

        for (SmaliLinkRepresentation link : mth.getReverseLinks()) {
            SmaliClassRepresentation nextCls = apk
                    .getClass(link.getLinkClass());

            if (nextCls == null)
                continue; // TODO:throw error?

            SmaliMethodRepresentation nextMth = nextCls.getMethod(link
                    .getLinkMethod());

            // register tags for each of the methods this method connects to.
            propagateTagsForHoneyPot(apk, tags, nextMth);
        }

		/*
		 * for (SmaliLinkRepresentation link : mth.getReverseLinks()) {
		 * SmaliClassRepresentation nextCls = apk
		 * .getClass(link.getLinkClass()); if (nextCls == null) continue; //
		 * TODO:throw error?
		 *
		 * SmaliMethodRepresentation nextMth = nextCls.getMethod(link
		 * .getLinkMethod());
		 *
		 * // register tags for each of the methods this method connects to.
		 * propagateTagsForHoneyPot(apk, tags, nextMth); }
		 */
    }

	/*
	 * This method iterates over the methods of all classes of the apk for each
	 * found link (i.e. method call on another object) it notifies the other
	 * method who is accessing it (for doing reverse trees).
	 *
	 * A special case includes the access to methods defined in superclasses.
	 * for this special case we verify the existence of the superclass methods
	 * and register the links in the class itself instead of in the method
	 * (which is non-existing for this class)
	 */

    private static void processReverseLinks(SmaliHoneyPots honeyPots,
                                            SmaliApkRepresentation apk) {
        if (apk == null) {
            doLog("reverseLinks : apk was null");
            return;
        }

        // This method is runned once per apk, we dont want the links of the
        // previous apk
        honeyPots.clearHoneyPotLinks();

        // iterate over all package links to reverse the links
        for (SmaliClassRepresentation cls : apk.getClasses()) {
            for (SmaliMethodRepresentation mth : cls.getMethods()) {
                for (SmaliLinkRepresentation link : mth.getLinks()) {

                    // Attempt to retrieve the class the link refers to
                    SmaliClassRepresentation addcls = apk.getClass(link
                            .getLinkClass());

                    if (addcls != null) { // found the class
                        SmaliMethodRepresentation addmth = addcls
                                .getMethod(link.getLinkMethod());

                        if (addmth != null) { // found the method

                            processReverseLink(apk, addmth, link, cls, mth);

                        } else { // Could not find the method (i.e. not in smali
                            // file)

                            // Lets check if the method is defined in the
                            // superclass
                            processReverseSuperClass(apk, cls, mth, link);
                        }

                    } else {
                        // Class was not found, lets check if it is a
                        // honeyPot/Sink

                        processReverseHoneyPot(honeyPots, apk, cls, mth, link);
                    }
                }
            }
        }
    }

    private static void processReverseLink(SmaliApkRepresentation apk,
                                           SmaliMethodRepresentation addmth, SmaliLinkRepresentation link,
                                           SmaliClassRepresentation cls, SmaliMethodRepresentation mth) {
        // We register on the other method that we called it
        // (from which class and method) :)
        addmth.createAccessToThisMethod(new SmaliLinkRepresentation(apk
                .getNameEncoding(), cls.getClassID(), mth.getMethodID()));
        doLog("processReverseLink : Successfully reversed link "
                + link.getLinkClass() + "." + link.getLinkMethod());
    }

    private static void processReverseSuperClass(SmaliApkRepresentation apk,
                                                 SmaliClassRepresentation cls, SmaliMethodRepresentation mth,
                                                 SmaliLinkRepresentation link) {

        String superClsName = cls.getSuperClassName();
        SmaliClassRepresentation superCls = apk.getClass(superClsName);

        if (superCls == null) // not in smali and cant find superclass
            doLog("reverseLinks : Could not find method " + link.getLinkClass()
                    + "." + link.getLinkMethod()
                    + " -> superclass not in apk : " + superClsName);

        else { // superclass belonged to the apk :D

            SmaliMethodRepresentation superMth = superCls.getMethod(link
                    .getLinkMethod());

            if (superMth != null) {
                doLog("reverseLinks : found method " + link.getLinkClass()
                        + "." + link.getLinkMethod() + " in super class "
                        + superClsName);
                cls.createAccessToThisClassSuperMethods(new SmaliLinkRepresentation(
                        apk.getNameEncoding(), cls.getClassID(), mth
                        .getMethodID()));
                doLog("reverseLinks : Successfully reversed link "
                        + link.getLinkClass() + "." + link.getLinkMethod());

            } else { // This method probably belonged to the superclass of the
                // superclass
                // TODO: support multiple levels of superclass searching!!
                doLog("processReverseSuperClass : method "
                        + link.getLinkClass() + "." + link.getLinkMethod()
                        + " not found in super class " + superClsName);
            }
        }
    }

    private static void processReverseHoneyPot(SmaliHoneyPots honeyPot,
                                               SmaliApkRepresentation apk, SmaliClassRepresentation cls,
                                               SmaliMethodRepresentation mth, SmaliLinkRepresentation link) {

        SmaliClassRepresentation honeyCls = honeyPot.getSmaliClass(link
                .getLinkClass());

        if (honeyCls != null) { // class was loaded as an honeyPot class

            doLog("reverseLinks : found honeyPot Class!");
            SmaliMethodRepresentation honeyMth = honeyCls.getMethod(link
                    .getLinkMethod());

            if (honeyMth != null) { // method was loaded in the honeyPot class

                honeyMth.createAccessToThisMethod(new SmaliLinkRepresentation(
                        apk.getNameEncoding(), cls.getClassID(), mth
                        .getMethodID()));

            } else {
                // Although the honey class was present, the method was not
                // yet present. Lets check if it belongs to PackageCategories

                // doLog("reverseLinks : could not find honeyPot Method!");
                processPackageCategoryHoneyPot(honeyPot, apk, cls, mth, link);
            }
        } else {
            // not found and not an honeyPot class, lets check if it
            // belongs to a honeyPot package :)

            processPackageCategoryHoneyPot(honeyPot, apk, cls, mth, link);
        }
    }

    private static void dumpClasses(){

    }

    private static void processPackageCategoryHoneyPot(SmaliHoneyPots honeyPot,
                                                       SmaliApkRepresentation apk, SmaliClassRepresentation cls,
                                                       SmaliMethodRepresentation mth, SmaliLinkRepresentation link) {
        // Check if it is ignorable - i.e. read from android.ignore file
        if (!PackageCategories.shouldIgnoreMethod(apk.getNameEncoding(),
                link.getLinkClass(), link.getLinkMethod())) {

            // Check if this package is tagged in the PackageCategories
            ArrayList<String> tags = PackageCategories.getTagsPerClass(
                    apk.getNameEncoding(), link.getLinkClass());

            if (tags == null) { // Non existent class
                doLog("reverseLinks : Could not find class (not honeyPot as well) - "
                        + link.getLinkClass());

            } else { // Should register this class and method in the honey
                // Pots!!
                SmaliMethodRepresentation newHoneyMth = honeyPot
                        .registerKnownMethod(link.getLinkClass(),
                                link.getLinkMethod());

                // If this method occurs and is registered in
                // PackageCategories,
                // then its because it has tags! So lets register them
                newHoneyMth._accesses.registerTags(tags);

                // Now reverse the links!
                newHoneyMth
                        .createAccessToThisMethod(new SmaliLinkRepresentation(
                                apk.getNameEncoding(), cls.getClassID(), mth
                                .getMethodID()));

                doLog("Created and registered link : " + link.getLinkClass()
                        + "." + link.getLinkMethod());
            }
        }
    }

}
