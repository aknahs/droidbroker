package com.aknahs.droidbroker;

import java.util.HashSet;

import com.google.gson.annotations.Expose;

public class CompressedClass {
    public Integer _classID;
    @Expose
    public HashSet<Integer> _interactClasses = new HashSet<Integer>();

    public CompressedClass(Integer id){
        _classID = id;
    }
}
