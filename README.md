Static analysis tool for Android applications that generates json representations of an app and provides java APIs for retrieving its information.

It is responsible for interpreting applications and identifying its resources (static analysis). 

It decompiles applications and parses them to gather the existing classes, methods, relations (method invocation patterns), activities, broadcasters and statistical information. 

It outputs a json representation of the package - check [DroidSmali project](https://bitbucket.org/aknahs/droidsmali), traces specific logs (e.g. access to Sockets) and uses the relations to export a .dot representation of the application call graph.


## How to use

To use DroidBroker download the executable jar and place the application apk in the apks folder.
DroidBroker will decompile and process all the applications in this folder.
The normal usage is as follows:

```bash
$ wget https://bitbucket.org/aknahs/droidbroker/downloads/droidbroker-0-2-1.tar.gz
$ java -jar DroidBroker.java
```

Additionally you can provide some optional arguments:

* -a - define the apktool path
* -G - generate .dot and gephi graphs
* -P - read the packages DSL file
* -g - do not generate .dot files

## DSL packages

DroidBroker allows you to tag methods/classes that can trigger a method invocation to specific package classes.
To do so it provides a DSL language to define tags and packages to detect.

The DSL file we use is present in this repository (android.packages).

For example, to detect accesses to the Android location packages, the following entry could be registered:

```
>android.location
-hardware
-gps 	
	#Contains the framework API classes that define Android location-based and related services.
```

As you might have noticed, package names are preceded with ">". 

Tags can be any string with no spaces (e.g. -hardware, -localfiles, -ui, -gps, -internet, -infosource, -os, -reflection, -threading).

The description of the package is added using the "#" character.

## Generated graphs

As an example, the following graph represents the method interactions in skype:

![skype-overall.png](https://bitbucket.org/repo/bo4peM/images/2306416055-skype-overall.png)


Graphs for DSL tagged package accesses are also graphically represented. The following image shows the methods that trigger hardware accesses:

 ![skype-hardware.png](https://bitbucket.org/repo/bo4peM/images/1657311743-skype-hardware.png)

Finally, a more fine-grained tag filter - the use of GPS APIs.


  ![gps-example.png](https://bitbucket.org/repo/bo4peM/images/48361551-gps-example.png)